"""
Woggle - bot framework

  Copyright (C) 2021 Yorick Bosman <yorick@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""


def sanick(bot, oldnick, newnick):
    """ Rename a user """
    bot.write(["SANICK", oldnick, newnick])


def sajoin(bot, nick, channel):
    """ Join a user to a channel """
    bot.write(["SAJOIN", nick, channel])


def sapart(bot, nick, channel, reason=""):
    """ Part a user from a channel, with optional reason """
    bot.write(["SAPART", nick, channel, reason])


def samove(bot, nick, fromchannel, tochannel):
    """ Move a user from one channel to another """
    bot.write(["SAMOVE", nick, fromchannel, tochannel])
