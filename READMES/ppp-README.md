# Pim Pam Pet

Pim Pam Pet is a Dutch party game, which can be quite spammy, but is very fun to play late at night with the older scouts (who can type fast enough for this game)

## Game play
Onces the game is started it will give a category and a letter. You need to come up with an example of those combinates.

### Example
Category: Scout related things
Letter: S

Possible answers:
 - Scoutlink
 - Scouting
 - Super boring hikes

## How to run
To start and stop you need the `ppp_control` permissions

To start a game use the command `.pppstart <#channel>`. This will start the game in that channel. Be sure to have ops there, it can get spammy.

To stop use `.pppstop <#channel>` and it will shut down

## Awarding the users

To award points you need the `ppp_points` permissions
To award a player send a PM to the bot with `.pppaward <#channel> <user>`
You can also deduct points with `.pppdeduct <#channel> <user>`

## Get your points
Say `.ppppoints` in the channel, and the bot will say your point amount

### Not yet implemented, will be done soon!
A thing to reguarly print the highscores in the chat