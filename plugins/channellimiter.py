"""
Woggle - bot framework

  Copyright (C) 2024 Mobius <18geeo@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin

from helpers import auth, sa


class ChannelLimiter:

    """

    [channellimiter]
    channels = #chan1, #chan2, #chan3
    maxchannels = 1
    part_notice = Please do not join too many similar channels

    [permissions]
    channellimiterbypass = thijseigenwijs
    """

    def __init__(self, bot):
        print("[ChannelLimiter] Initializing plugin")
        # TODO: is there a inbuilt function for this? seems like something that needs to be done regually
        self.channels = bot.config.channellimiter.channels.translate(
            str.maketrans("", "", " \n\t\r")
        ).split(",")
        self.maxchannels = int(bot.config.channellimiter.maxchannels)


def setup(bot):
    global CHANNELLIMITER
    CHANNELLIMITER = ChannelLimiter(bot)


@plugin.event("JOIN")
def join(bot, trigger):
    global CHANNELLIMITER
    current_channel = trigger.sender
    user_nick = trigger.nick
    # not not affect the bot
    if user_nick == bot.nick:
        return

    # check if this channel is limited
    if not (current_channel in CHANNELLIMITER.channels):
        return

    # is the user in the ops channels?
    if user_nick in bot.channels[bot.config.JotiGeneral.ops_channel].users:
        return

    # by default does not include the channel the user just joined so add that manually
    channels_user_is_in = [current_channel]

    for chan in CHANNELLIMITER.channels:
        if chan in bot.channels and (user_nick in bot.channels[chan].users):
            channels_user_is_in.append(chan)

    # is the user in too many channels that are limited
    if len(channels_user_is_in) > CHANNELLIMITER.maxchannels:
        # most users are not admins, therefore more efficient to do check after checking.
        if auth.account_has_permission(bot, trigger.account, "channellimiterbypass"):
            return

        # remove them from the one they just tried to join
        sa.sapart(bot, user_nick, current_channel)
        bot.notice(bot.config.channellimiter.part_notice, user_nick)
