"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from helpers import auth
from sopel import plugin

class ChanControl:
    """
    This module can be used to control the different channels under the
    control of this bot

    Config:
        [JotiGeneral]
        ops_channel = #beneluxops
        bot_channel = #dutch-bots
        coord = max
        cos = "Thijseigenwijs, Vaandrig"
        ops = "Suikerklontje, zeilertje"
        chanserv = chanserv
        operserv = operserv
        corights = "+votsrihqaRAF"
        oprights = "+varioe"
        suprights = "+Vvi"
        channels = #dutch01, #dutch02, #dutch03, #dutch-test

        [chancontrol]
        quizbot = EA
        quizchan = #dutch-quiz
        quiztopic = "Quizbot is online!"
        wolfbot = Burgemeester
        wolfchan = #dutch-wakkerdam
        wolftopic = "Je kunt weer weerwolven!"
        unobot = Duno
        unochan = #dutch-uno
        unotopic = "Uno is online!"
        pppbot = Pimmetje
        pppchan = #dutch-pimpampet
        ppptopic = "Pimpampet is online!"
        flagsopenp = "+ntTABlJf 80 10 5:3"
        flagsopeng = "+ntTABlJ 75 10"
        flagsopenn = "-cSmspki"
        flagsclosingp = "+ntsmil 1"
        flagsclosingn = "-A"
        opentopic = "Dit kanaal is open!"
        closetopic = "Dit kanaal is gesloten!"

        [permissions]
        chancontrol = Thijseigenwijs, vaandrig, max


        Explaination of variables.

        flagsopenp are the flags that are set to an opening channel, positive
        flagsopenn are the flags that needs to be removed on opening a channel
        flagsopeng are the flags that are set to open a gamechannel

    """

    def __init__(self, bot):
        """
        Setup certain things for the module (Getting some configs)
        """
        self.quizbot = bot.config.chancontrol.quizbot
        self.quizchan = bot.config.chancontrol.quizchan
        self.quiztopic = bot.config.chancontrol.quiztopic

        self.wolfbot = bot.config.chancontrol.wolfbot
        self.wolfchan = bot.config.chancontrol.wolfchan
        self.wolftopic = bot.config.chancontrol.wolftopic

        self.unobot = bot.config.chancontrol.unobot
        self.unochan = bot.config.chancontrol.unochan
        self.unotopic = bot.config.chancontrol.unotopic

        self.pppbot = bot.config.chancontrol.pppbot
        self.pppchan = bot.config.chancontrol.pppchan
        self.ppptopic = bot.config.chancontrol.ppptopic

        self.flagsopenp = bot.config.chancontrol.flagsopenp
        self.flagsopenn = bot.config.chancontrol.flagsopenn
        self.flagsopeng = bot.config.chancontrol.flagsopeng
        self.flagsclosingp = bot.config.chancontrol.flagsclosingp
        self.flagsclosingn = bot.config.chancontrol.flagsclosingn

        self.opentopic = bot.config.chancontrol.opentopic
        self.closetopic = bot.config.chancontrol.closetopic


    def open_channel(self, bot, trigger):
        """
        The function to open a channel
        """
        if not trigger.group(2).startswith("#"):
            bot.say("OPENING | You must supply a channel, starting with a #")
            return

        if trigger.group(2) is None:
            bot.say("OPENING | You must supply a channel")

        if trigger.group(2) is self.quizchan:
            bot.write(["MODE", self.quizchan, self.flagsopeng])
            bot.write(["MODE", self.quizchan, self.flagsopenn])
            bot.write(["TOPIC", self.quizchan + " :" + self.quiztopic])

        elif trigger.group(2) is self.wolfchan:
            bot.write(["MODE", self.wolfchan, self.flagsopeng])
            bot.write(["MODE", self.wolfchan, self.flagsopenn])
            bot.write(["TOPIC", self.wolfchan + " :" + self.wolftopic])

        elif trigger.group(2) is self.unochan:
            bot.write(["MODE", self.unochan, self.flagsopeng])
            bot.write(["MODE", self.unochan, self.flagsopenn])
            bot.write(["TOPIC", self.unochan + " :" + self.unotopic])

        elif trigger.group(2) is self.pppchan:
            bot.write(["MODE", self.pppchan, self.flagsopeng])
            bot.write(["MODE", self.pppchan, self.flagsopenn])
            bot.write(["TOPIC", self.pppchan + " :" + self.ppptopic])

        elif trigger.group(2) in bot.channels:
            bot.write(["MODE", trigger.group(2), self.flagsopenp])
            bot.write(["MODE", trigger.group(2), self.flagsopenn])
            bot.write(["TOPIC", trigger.group(2) + " :" + self.opentopic])

        else:
            bot.say("OPENING | {} is not found".format(trigger.group(2)))
            bot.say(
                "OPENING | {} tried to open {} but it doesn't exsist".format(
                    trigger.nick, trigger.group(2)
                ),
                bot.config.chancontrol.bot_channel,
            )
            return

        bot.say(
            "OPENING | {} has opened {}".format(trigger.nick, trigger.group(2)),
            bot.config.chancontrol.bot_channel,
        )
        bot.say("OPENING | {} is now open".format(trigger.group(2)))

    def close_channel(self, bot, trigger):
        """
        The function to close a channel
        """
        if not trigger.group(2).startswith("#"):
            bot.say("CLOSING | You must supply a channel, starting with a #")
            return

        if trigger.group(2) is None:
            bot.say("OPENING | You must supply a channel")

        if trigger.group(2) is self.quizchan:
            bot.write(["MODE", self.quizchan, self.flagsclosingp])
            bot.write(["MODE", self.quizchan, self.flagsclosingn])
            bot.write(["TOPIC", self.quizchan + " :" + self.closetopic])

        elif trigger.group(2) is self.wolfchan:
            bot.write(["MODE", self.wolfchan, self.flagsclosingp])
            bot.write(["MODE", self.wolfchan, self.flagsclosingn])
            bot.write(["TOPIC", self.wolfchan + " :" + self.closetopic])

        elif trigger.group(2) is self.unochan:
            bot.write(["MODE", self.unochan, self.flagsclosingp])
            bot.write(["MODE", self.unochan, self.flagsclosingn])
            bot.write(["TOPIC", self.unochan + " :" + self.closetopic])

        elif trigger.group(2) is self.pppchan:
            bot.write(["MODE", self.pppchan, self.flagsclosingp])
            bot.write(["MODE", self.pppchan, self.flagsclosingn])
            bot.write(["TOPIC", self.pppchan + " :" + self.closetopic])

        elif trigger.group(2) in bot.channels:
            bot.write(["MODE", trigger.group(2), self.flagsclosingp])
            bot.write(["MODE", trigger.group(2), self.flagsclosingn])
            bot.write(["TOPIC", trigger.group(2) + " :" + self.closetopic])

        else:
            bot.say("CLOSING | {} is not found".format(trigger.group(2)))
            bot.say(
                "CLOSING | {} tried to open {} but it doesn't exsist".format(
                    trigger.nick, trigger.group(2)
                ),
                bot.config.chancontrol.bot_channel,
            )
            return

        bot.say(
            "CLOSING | {} has opened {}".format(trigger.nick, trigger.group(2)),
            bot.config.chancontrol.bot_channel,
        )
        bot.say("CLOSING | {} is now closed".format(trigger.group(2)))


CHANCONTROL = None


def setup(bot):
    """
    Setup the bot
    """
    global CHANCONTROL  # pylint: disable=global-statement
    CHANCONTROL = ChanControl(bot)


@plugin.commands("openen")
@plugin.example(".openen <channel>")
@auth.require_permission("chancontrol")
def openen(bot, trigger):
    """
    Open a channel
    """
    global CHANCONTROL  # pylint: disable=global-statement
    CHANCONTROL.open_channel(bot, trigger)

@plugin.commands("sluiten")
@plugin.example(".sluiten <channel>")
@auth.require_permission("chancontrol")
def clsoing(bot, trigger):
    """
    Open a channel
    """
    global CHANCONTROL  # pylint: disable=global-statement
    CHANCONTROL.close_channel(bot, trigger)
