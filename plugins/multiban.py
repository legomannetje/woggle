"""
Woggle - bot framework

  Copyright (C) 2024 Mobius <18geeo@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin

from helpers import auth


class MultiBan:
    """
    [multiban]
    excemptchans = #chanops

    [permissions]
    multiban = Thijseigenwijs
    """

    def multiban_add(self, bot, trigger):
        host = trigger.group(3)
        time = trigger.group(4)

        for chan in bot.channels:
            if not chan in bot.config.multiban.excemptchans:
                if time:
                    bot.write(["TBAN", chan, time, host])
                else:
                    bot.write(["MODE", chan, "+b", host])

        if time:
            bot.say("Added timed multi-ban for " + host + " for " + time)
        else:
            bot.say("Added multi-ban for " + host)

    def multiban_remove(self, bot, trigger):
        host = trigger.group(3)

        for chan in bot.channels:
            if not chan in bot.config.multiban.excemptchans:
                bot.write(["MODE", chan, "-b", host])
        bot.say("Removed multi-ban for " + host)


MULTIBAN = None


def setup(bot):
    global MULTIBAN
    MULTIBAN = MultiBan()


@plugin.commands("multiban add")
@auth.require_permission("multiban")
def register(bot, trigger):
    global MULTIBAN
    MULTIBAN.multiban_add(bot, trigger)


@plugin.commands("multiban remove")
@auth.require_permission("multiban")
def deregister(bot, trigger):
    global MULTIBAN
    MULTIBAN.multiban_remove(bot, trigger)
