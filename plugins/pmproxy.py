"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin

@plugin.rule(".*")
@plugin.require_privmsg
def pmproxy(bot, trigger):
    """
    Forward all the PM's to the bot channel or ops channel

    [pmproxy]
    target = #dutchbots
    prefix = [PM]
    auto_reply = None
    """
    private_message = f"{trigger}"
    if private_message:
        bot.say(
            f"{bot.config.pmproxy.prefix} {trigger.nick} - {private_message}",
            bot.config.pmproxy.target,
        )

        bot.say(bot.config.pmproxy.auto_reply, trigger.nick)
