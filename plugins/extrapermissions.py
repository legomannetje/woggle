"""
Woggle - bot framework

  Copyright (C) 2024 Mobius <18geeo@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sopel.plugin as plugin
import re

from helpers import timer, auth

class ExtraPermissions:
    
    """
    
    """
    
    
    def __init__(self, bot):
        flagsTimer(bot).start()

def setup(bot): 
    ExtraPermissions(bot)

@timer.resetable_timer(10)
def flagsTimer(bot):
    rtimer = flagsTimer(bot)
    rtimer.reset(True, int(bot.config.extrapermissions.groupserv_interval))
    allGroups = []
    for key, rawpermissions in vars(bot.config.permissions).items():
        if key.startswith("_"):
            continue
        
        for permission in [item.strip() for item in rawpermissions.split(",")]:
            if permission.startswith("!") and not (permission in allGroups):
                allGroups.append(permission)

    for group in allGroups:
        bot.say(f"flags {group}", "GroupServ")

@plugin.event("NOTICE")
def onNotice(bot, trigger):
    if not (trigger.sender == "GroupServ"):
        return
    group = re.search(r"(?<=End\sof\s)\![\_\w-]+(?=\sFLAGS\slisting)", trigger.plain)
    
    if group != None:
        auth.clearGroupFlags(group[0])
        return
    
    account = re.findall(r"\d[\s]+\s(\w+)\s+\+\w+", trigger.plain)
    if len(account) > 0:
        flags = re.findall(r"\d[\s]+\s\w+\s+(\+\w+)$", trigger.plain)
        auth.pushGroupFlags(account[0], flags[0])