"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin
from helpers import auth, sa, timer
import re

class Redirector:
    """
    
    [redirect]
    
    """
    def __init__(self, bot):
        self.queue = []
        self.bot = bot
        self.maxusers=int(bot.config.redirect.maxusers)
        self.queue_timer = queue_timer(bot)
        self.queue_timer.reset(True, int(5))
        self.enabled = False

    def getNextChan(self, bot, current_channel):
        # Extract the prefix from the current channel (everything before the last digit or underscore)
        match = re.match(r'^(.*?)[\d_]*$', current_channel)
        
        if match and not re.search(r'\d$', current_channel):
            prefix = match.group(1)
            
            # Find all channels that match the prefix and end in a number
            channels = {}
            for chan in bot.channels:
                if re.match(f'^{re.escape(prefix)}\\d+$', chan):
                    # Check if the channel is muted (+m)
                    if not bot.channels[chan].modes.get('m', False):
                        channels[chan] = len(bot.channels[chan].users)

            lowestUsers = 9999999
            lowestChannel = current_channel

            for chan in channels:
                if channels[chan] < lowestUsers:
                    lowestChannel = chan
                    lowestUsers = channels[chan]
                    
            if lowestUsers > self.maxusers or len(self.queue) > 0:
                return "queue"

            return lowestChannel
        return current_channel
            


    def add_to_queue(self, nick):
        self.queue.append(nick)

    def pop(self, bot):
        nick = self.queue.pop(0)
        if nick in bot.users:
            return nick
        return self.pop(bot)
    
    def queue_loop(self, bot):
        prefix = bot.config.redirect.mainchan
            
        # Find all channels that match the prefix and end in a number
        channels = {}
        for chan in bot.channels:
            if re.match(f'^{re.escape(prefix)}\\d+$', chan):
                # Check if the channel is muted (+m)
                users = len(bot.channels[chan].users)
                if users <= self.maxusers and not bot.channels[chan].modes.get('m', False):
                    channels[chan] = self.maxusers - users + 1

        for chan in channels:
            free = channels[chan]
            for i in range(free):
                if len(self.queue) > 0:
                    user = self.pop(bot)
                    sa.sapart(bot, user, bot.config.redirect.mainchan)
                    sa.sajoin(bot, user, chan)

        for i, user in enumerate(self.queue):
            bot.notice(f"You position in the queue is: {i + 1}", user)
        
    def handle_join(self, bot, nick, current_channel):
        if not self.enabled:
            return

        if nick == bot.nick or nick == bot.config.JotiGeneral.chanserv:
            return

        if nick in bot.channels[bot.config.JotiGeneral.ops_channel].users:
            return

        chan = self.getNextChan(bot, current_channel)
        # stop a join loop
        if chan == current_channel:
            return

        if chan[0] == "#":
            sa.sapart(bot, nick, current_channel)
            sa.sajoin(bot, nick, chan)
        elif chan == "queue":
            self.add_to_queue(nick)
            


global REDIRECTOR
REDIRECTOR = None
def setup(bot):
    global REDIRECTOR
    REDIRECTOR = Redirector(bot)


@plugin.event("JOIN")
@plugin.rule(".*")
def redirect(bot, trigger):
    global REDIRECTOR
    current_channel = trigger.sender
    user_nick = trigger.nick
    REDIRECTOR.handle_join(bot, user_nick, current_channel)

    


# TODO: nick change
@plugin.event("NICK")
@plugin.rule(".*")
def nick_change(bot, trigger):
    global REDIRECTOR
    try:
        index = REDIRECTOR.queue.index(trigger.nick)
        REDIRECTOR.queue[index] = trigger.args
    except (ValueError):
        return

@timer.resetable_timer(5)
def queue_timer(bot):
    global REDIRECTOR

    REDIRECTOR.queue_timer.reset(True, None)
    REDIRECTOR.queue_loop(bot)

@plugin.commands("redirector set")
@auth.require_permission("chancontrol")
def setonoff(bot, trigger):
    global REDIRECTOR
    arg = trigger.args[1][15:].lower().strip()

    set = None
    if arg == "on":
        set = True
    elif arg == "off":
        set = False
    else:
        return bot.reply("Usage: redirector set <on/off>", trigger.sender, trigger.nick)
    
    REDIRECTOR.enabled = set
    bot.reply(f"Set redirect enabled to {set}", trigger.sender, trigger.nick)


@plugin.commands("redirector setmax")
@auth.require_permission("chancontrol")
def setmax(bot, trigger):
    global REDIRECTOR

    arg = trigger.args[1][18:].lower().strip()

    try:
        maxusers = int(arg)
    except ValueError:
        return bot.reply("Error: argument was not an integer. Usage: redirector setmax <number>", trigger.sender, trigger.nick)
    REDIRECTOR.maxusers = maxusers
    bot.reply(f"Set max users to {maxusers}", trigger.sender, trigger.nick)


@plugin.commands("redirector collect")
@auth.require_permission("chancontrol")
def collect_users(bot, trigger):
    global REDIRECTOR
    channel = bot.channels[bot.config.redirect.mainchan]
    users = channel.users
    for user in users:
        REDIRECTOR.handle_join(bot, user, bot.config.redirect.mainchan)
