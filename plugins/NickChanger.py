"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re
from sopel import module

class nickChanger():
    """
    This module prevents users from changing their nicks to bad words.

    [nickChanger]
    newNicks = ScoutyMcScoutface, BraveScout, NiceScout, PathFinder, TreeClimber, BadgeBandit 

    [permissoins]
    nickchange = thijseigenwijs
    """
    
    badname_patterns = ["\blego\w*\b"]
    
    def __init__(self, bot):
        return

    # Function to check if a nickname matches any bad pattern
    def is_bad_nickname(self, nickname):
        for pattern in self.badname_patterns:
            if re.search(pattern, nickname):
                return True
        return False

    def onJoin(bot, trigger):
        return
    
    def onNick(bot, trigger):
        return
    
    def add(bot, trigger):
        return
    
    def remove(bot, trigger):
        return
    
    def listPatterns(bot, trigger):
        return


NICKCHANGER = None

def setup(bot):
    global NICKCHANGER 
    NICKCHANGER = nickChanger(bot)

# This function checks the nickname when a user joins a channel
@module.event('JOIN')
@module.rule('.*')
def check_nick_on_join(bot, trigger):
    global NICKCHANGER 
    # Get the user's nickname from the trigger object
    nickname = trigger.nick
    # Perform the regex check on the nickname
    if NICKCHANGER.is_bad_nickname(nickname):
        bot.say(f'{nickname}, your nickname is inappropriate. Please change it.')

# This function checks the nickname when a user changes their nickname
@module.event('NICK')
@module.rule('.*')
def check_nick_on_nickchange(bot, trigger):
    # Get the new nickname from the trigger object
    new_nick = trigger.nick
    # Perform the regex check on the new nickname
    if NICKCHANGER.is_bad_nickname(new_nick):
        bot.say(f'{new_nick}, your nickname is inappropriate. Please change it.')
