"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin
from helpers import auth

IRC_COLORS = [
    '04',  # Red
    '07',  # Orange
    '08',  # Yellow
    '09',  # Green
    '11',  # Light green
    '10',  # Cyan
    '12',  # Blue
    '02',  # Navy
    '06',  # Purple
    '13',  # Magenta
]

def rainbow_text(text):
    """Generate rainbow-colored text."""
    colored_text = ''
    color_count = len(IRC_COLORS)

    for i, char in enumerate(text):
        color_code = IRC_COLORS[i % color_count]
        colored_text += f'\x03{color_code}{char}'

    return colored_text

@plugin.command('rainbow')
@plugin.example('.rainbow This is a rainbow text!')
@auth.require_permission("rainbow")
def rainbow(bot, trigger):
    """
    Print the given text in rainbow colors.
    Usage: .rainbow <text>
    """
    text = trigger.group(2)
    if not text:
        bot.reply("You need to provide some text to print in rainbow style.")
        return

    rainbow_message = rainbow_text(text)
    bot.say(rainbow_message)