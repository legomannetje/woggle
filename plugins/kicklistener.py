"""
Woggle - bot framework

  Copyright (C) 2024 Mobius <18geeo@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin
from time import time


class KickListener:
    
    """
    
    """
    def __init__(self, bot):
        self.kicks = {}
    
    def onKick(self,bot,trigger):
        hostmask = trigger.nick + "!" + trigger.user + "@" + trigger.host

        # create the hostmask list if it doesn't exist
        if (hostmask in self.kicks):
            self.kicks[hostmask].append(time())
        else:
            self.kicks[hostmask] = [time()]

        if len(self.kicks[hostmask]) >= int(bot.config.kicklistener.kickamount):
            bot.say("Host: " 
                    + hostmask + " has been kicked " 
                    + str(len(self.kicks[hostmask])) + " times in the last " 
                    + bot.config.kicklistener.timeout + " minutes", 
            bot.config.JotiGeneral.ops_channel)

    def clean(self, bot):
        timeoutTime = time() - int(bot.config.kicklistener.timeout)*60
        for host in self.kicks:
            self.kicks[host] = list(filter(lambda t: t < timeoutTime, self.kicks[host]))


KICKLISTENER = None

def setup(bot):
    global KICKLISTENER
    KICKLISTENER = KickListener(bot)

@plugin.event("KICK")
def kick_event(bot, trigger):
    global KICKLISTENER
    KICKLISTENER.onKick(bot, trigger)

@plugin.interval(60)
def clean(bot):
    global KICKLISTENER
    KICKLISTENER.clean(bot)