"""
Woggle - bot framework



This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin
from helpers import auth

@auth.require_permission("actasbot")
@plugin.commands('say')
def say(bot, trigger):
    """Make the bot say something in the specified channel from a PM."""
    if not trigger.group(2):
        bot.reply("Usage: .say #channel message")
        return

    parts = trigger.group(2).split(' ', 1)
    if len(parts) < 2:
        bot.reply("You need to specify both a channel and a message.")
        return
    
    channel, message = parts
    if channel.startswith('#'):  # Ensure it's a valid channel
        bot.say(message, channel)
    else:
        bot.reply("Invalid channel. Please start with '#'.")

@auth.require_permission("actasbot")
@plugin.commands('do')
def do(bot, trigger):
    """Make the bot perform an action in the specified channel from a PM."""
    if not trigger.group(2):
        bot.reply("Usage: .do #channel action")
        return

    parts = trigger.group(2).split(' ', 1)
    if len(parts) < 2:
        bot.reply("You need to specify both a channel and an action.")
        return
    
    channel, action = parts
    if channel.startswith('#'):  # Ensure it's a valid channel
        bot.action(action, channel)
    else:
        bot.reply("Invalid channel. Please start with '#'.")