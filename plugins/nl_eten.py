"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from helpers import auth
from sopel import plugin
import random
    
@plugin.commands("oranje", "bitter", "tompoes", "koning")
def oranje(bot, trigger):
    bot.action(
        "geeft {} een oranjebitter en een dikke tompoes".format(getName(trigger))
    )


@plugin.commands("chocola")
def chocola(bot, trigger):
    bot.action(
        "geeft {} een grote reep tony chocolony chocolade!".format(getName(trigger))
    )


@plugin.commands("taart")
def taart(bot, trigger):
    slagroom = random.choice([" met een toefje slagroom", "", ""])
    bot.action(
        "geeft {} een groot stuk appeltaart{}!".format(getName(trigger), slagroom)
    )


@plugin.commands("koekje")
def koekje(bot, trigger):
    cookie_type = random.choice(["Chocolate Chip ", "Oreo ", "Glace ", "", "Nijntje "])
    size = random.choice(["klein", "normaal", "grote", "enorme"])
    flavor = random.choice(["lekker", "", "verrukelijk"])
    method = random.choice(["maakt voor", "geeft aan", "koopt voor"])
    bot.action(
        "{} {} een {} {} {}koek!".format(
            method, getName(trigger), flavor, size, cookie_type
        )
    )


@plugin.commands("frikandel")
def frikandel(bot, trigger):
    bot.action(
        "gaat het tostiijzer wel even aanzetten voor {}".format(getName(trigger))
    )

@plugin.commands("chips")
def chips(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een zakje chips".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een zakje chips".format(trigger.nick))


@plugin.commands("pindas")
def pindas(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een bakje pinda's".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een bakje pinda's".format(trigger.nick))


@plugin.commands("bifi")
def bifi(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een heerlijk bifi worstje".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een heerlijk bifi worstje...".format(trigger.nick))


@plugin.commands("broodjebal")
def broodjebal(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een dampend broodje bal! Eet smakelijk".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een dampend broodje bal! Eet smakelijk".format(trigger.nick)
        )


@plugin.commands("tortillachips")
def tortillachips(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een bakje tortilla chips... Probeer ook eens de !salsadip".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een bakje tortilla chips... Probeer ook eens de !salsadip".format(
                trigger.nick
            )
        )


@plugin.commands("mars")
def mars(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een Mars reep".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een Mars reep".format(trigger.nick))


@plugin.commands("twix")
def twix(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action("geeft {} namens {} een Twix".format(trigger.group(2), trigger.nick))
    else:  # Give to yourself
        bot.action("geeft {} een Twix".format(trigger.nick))


@plugin.commands("kinderbueno")
def kinderbueno(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een kinderbueno".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een kinderbueno".format(trigger.nick))


@plugin.commands("bittergarnituur")
def bittergarnituur(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een bittergarnituur... Saus er bij?".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een bittergarnituur... Saus er bij?".format(trigger.nick))


@plugin.commands("tosti", "tostie")
def tosti(bot, trigger):

    if int(random.random() * 100) == 50:
        bot.action(
            "sorry {}, er ligt een frikandel in het tosti-ijzer, dus je moet even wachten!".format(
                trigger.nick
            )
        )
        return

    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een lekker tosti... Pas op de kaas is erg heet!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een lekker tosti... Pas op de kaas is erg heet!".format(
                trigger.nick
            )
        )


@plugin.commands("kaas")
def kaas(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een boel blokjes kaas. Ook lekker met de salsadip?".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een boel blokjes kaas. Ook lekker met de salsadip".format(
                trigger.nick
            )
        )


@plugin.commands("salsadip", "salsa")
def salsa(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} eeen bakje salsadip".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een bak met salsadip".format(trigger.nick))


@plugin.commands("worst")
def worst(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een paar dikke plakken worst... Ook lekker met mosterd!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een paar dikke plakken worst... Ook lekker met mosterd!".format(
                trigger.nick
            )
        )


@plugin.commands("kaasenworst")
def kaasenworst(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            'geeft {} namens {} een bordje met kaas en worst... "Haha, dat doet me denken aan die vele verjaardagen van vroeger!"'.format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            'geeft {} een bordje met kaas en worst... "Haha, dat doet me denken aan die vele verjaardagen van vroeger!"'.format(
                trigger.nick
            )
        )

@plugin.commands("vlammetjes")
def vlammetjes(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een porsie vlammetjes... Kan iemand alvast de brandblusser voor het scherm houden?".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een porsie vlammetjes... Kan iemand alvast de brandblusser voor het scherm houden?".format(
                trigger.nick
            )
        )


@plugin.commands("bitterballen")
def bitterballen(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            'geeft {} namens {} een portie bitterballen... "Haha weet je nog? Ma Flodder in Flodder in America met haar bitterballs?"'.format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            'geeft {} een portie bitterballen... "Haha weet je nog? Ma Flodder in Flodder in America met haar bitterballs?"'.format(
                trigger.nick
            )
        )


@plugin.commands("borrelnootjes")
def borrelnootjes(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "zet voor {} een bakje borrelnootjes op de bar namens {} ... En niet uitzoeken!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "zet voor {} een bakje borrelnootjes op de bar... En niet uitzoeken!".format(
                trigger.nick
            )
        )


@plugin.commands("mayonaise", "mayo")
def mayonaise(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} een beetje mayonaise namens {}... Sonja bakker zei laatst dat mayonaise 75% tot 80% vet bevat!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een beetje mayonaise... Sonja bakker zei laatst dat mayonaise 75% tot 80% vet bevat!".format(
                trigger.nick
            )
        )


@plugin.commands("curry")
def curry(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} een beetje curry namens {}... Met echte stukjes Adam Curry uiteraard!!!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een beetje curry... Met echte stukjes Adam Curry uiteraard!!!".format(
                trigger.nick
            )
        )


@plugin.commands("ketchup")
def ketchup(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} een beetje ketchup namens {}... In het RVU programma Keuringsdienst van Waarde (20 oktober 2006) werd betoogd dat tomatenketchup gemiddeld 45 suikerklontjes per fles bevat.".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een beetje ketchup... In het RVU programma Keuringsdienst van Waarde (20 oktober 2006) werd betoogd dat tomatenketchup gemiddeld 45 suikerklontjes per fles bevat.".format(
                trigger.nick
            )
        )


@plugin.commands("suiker")
def suiker(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een zakje suiker...".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een zakje suiker...".format(trigger.nick))


@plugin.commands("pizza")
def pizza(bot, trigger):
    pizzarange = [
        "pizza hawaii",
        "pizza salami",
        "pizza quattro formage",
        "pizza margaritta",
        "pizza shoarma",
        "pizza fungi",
    ]
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een {}".format(
                trigger.group(2), trigger.nick, random.choice(pizzarange)
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een {}".format(trigger.nick, random.choice(pizzarange)))


@plugin.commands("friet")
def friet(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een frietje met".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een frietje met".format(trigger.nick))


@plugin.commands("kroket")
def kroket(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een kroket".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een kroket".format(trigger.nick))


"""
   Fris Bar
"""


@plugin.commands("water")
def water(bot, trigger):
    bot.action("geeft {} een groot glas koud water!".format(getName(trigger)))


@plugin.commands("chocomelk", "chocomel")
def chocomelk(bot, trigger):
    tempature = random.choice(
        ["warme", "hete", "warme", "warme", "hete", "warme", "warme"]
    )
    cream = random.choice(
        [" met een toefje slagroom", " met slagroom", "", "", " met een rietje", ""]
    )
    bot.action(
        "geeft {} een {} chocolademelk{}!".format(getName(trigger), tempature, cream)
    )


@plugin.commands("ijsthee", "icetea", "icethee")
def ijsthee(bot, trigger):
    bot.action(
        "geeft {} een lekker glas met koude IJsthee".format(getName(trigger))
    )

@plugin.commands("koffie")
def koffie(bot, trigger):
    temperature = random.choice(["hete", "warme"])
    coffee = random.choice(
        ["Cappuccino", "Espresso", "Koffie Verkeerd", "Koffie", "Senseo"]
    )
    chance = random.randint(0, 9)
    if chance == 0:
        bot.action(" Error 418: I'm a teapot!")
    else:
        bot.action("geeft {} een {} {}".format(getName(trigger), temperature, coffee))


@plugin.commands("thee")
def thee(bot, trigger):
    temperature = random.choice(["warme", "hete"])
    size = random.choice(["grootte", "normale", "kleine"])
    tea = random.choice(
        [
            "chai thee",
            "english breakfast",
            "Earl Grey",
            "slaapthee",
            "oolong thee",
            "groene thee",
            "yerba mate",
            "rooibos thee",
            "bubble thee",
            "thai thee",
        ]
    )

    bot.action("geeft {} een {} {} {}".format(getName(trigger), size, temperature, tea))


@plugin.commands("hoestdrank")
def hoestdrank(bot, trigger):
    bot.action("geeft {} een shotje hoestdrank".format(getName(trigger)))


@plugin.commands("ontsmettingsmiddel")
def ontsmettingsmiddel(bot, trigger):
    bot.action(
        "schenkt een glaasje ontsmettingsmiddel in voor {} . Wil je er ook een citroentje in?".format(
            getName(trigger)
        )
    )


@plugin.commands("koffiemelk")
def koffiemelk(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een cupje koffiemelk...".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een cupje koffiemelk...".format(trigger.nick))

@plugin.commands("lepeltje")
def lepeltje(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een lepeltje".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een lepeltje".format(trigger.nick))


@plugin.commands("melk")
def melk(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een beker melk...".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een beker melk...".format(trigger.nick))


@plugin.commands("cola")
def cola(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een colaatje...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een colaatje...".format(trigger.nick))


@plugin.commands("sinas")
def sinas(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een sinasje...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een sinasje...".format(trigger.nick))


@plugin.commands("7up", "sprite")
def sevenup(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een 7upje...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een 7upje...".format(trigger.nick))


@plugin.commands("cassis")
def cassis(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een cassis...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een cassis...".format(trigger.nick))


@plugin.commands("dubbelfris")
def dubbelfris(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een dubbelfris...".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("geeft {} een dubbelfris...".format(trigger.nick))


@plugin.commands("sparood")
def sparood(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een spa rood...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een spa rood...".format(trigger.nick))


@plugin.commands("spablauw")
def spablauw(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een spa blauw...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een spa blauw...".format(trigger.nick))


@plugin.commands("appelsap")
def appelsap(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een appelsapje".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een appelsapje".format(trigger.nick))


@plugin.commands("jus")
def jus(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een jus-je".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("geeft {} een jus-je".format(trigger.nick))


@plugin.commands("sinaasappelsap")
def sinaasappelsap(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "geeft {} namens {} een sinaasappelsapje (mooi woord voor de scrabble)".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "geeft {} een sinaasappelsapje (mooi woord voor de scrabble)".format(
                trigger.nick
            )
        )


@plugin.commands("fristi", "fristie")
def fristi(bot, trigger):
    bot.action("geeft {} een groot glas koude fristi!".format(getName(trigger)))


def getName(trigger):
    name = ""
    if trigger.group(2):  # Give cake to someone else
        name = trigger.group(2)
    else:  # Give to yourself
        name = trigger.nick

    return name
