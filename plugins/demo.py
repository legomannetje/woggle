"""
Woggle - bot framework

  Copyright (C) 2021 Christos Triantafyllidis <christos.triantafyllidis@gmail.com>
  Copyright (C) 2021 Thijs Tops <git@thijstops.com>
  Copyright (C) 2021 Yorick Bosman <yorick@gewoonyorick.nl>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from time import sleep

from sopel import db
from sopel.plugin import commands

from helpers import auth, timer


class Demo:
    """
    A class to demonstrate how to use classes and instances of them
    """

    def __init__(self):
        self.counter = 0

    def reset_counter(self, bot, sender):
        """
        Resets the counter to 0
        """
        self.counter = 0
        bot.say("The counter is now reset!", sender)


def setup(bot):
    """
    Initializes the bot
    """
    db_setup(bot)
    bot.memory["demo"] = Demo()


### Handling authentication
@commands("demo-account")
@auth.require_account
def demo_account(bot, trigger):
    """
    This will be executed ONLY if the user has identified to the IRC services
    """
    bot.reply(f"Trigger was from the identified user: {trigger.account}")


### Handling permissions
@commands("demo-permission")
@auth.require_permission("my_permission")
def demo_permissions(bot, trigger):
    """
    This will be executed ONLY if the user has identified to the IRC services
    and has the 'my_permission' permission
    """
    bot.reply(
        f"Trigger was from the identified user: {trigger.account} who has 'my_permission'"
    )


### Handling databases
CURRENT_SCHEMA_VERSION = 2
PLUGIN_NAME = __name__.split(".")[-1]


def db_setup(bot):
    """
    Initializes the database
    """
    upgrade_db(bot)


class Message(db.BASE):
    """
    SQLAlchemy class for messages
    """

    __tablename__ = f"{PLUGIN_NAME}_messages"
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(20))
    contents = db.Column(db.String(250))


def upgrade_db(bot):
    """
    Updates the DB schema
    """
    bot_db = db.SopelDB(bot.config)
    running_version = bot_db.get_plugin_value(PLUGIN_NAME, "db_version", default=0)

    if running_version == CURRENT_SCHEMA_VERSION:
        return

    if running_version < 1:
        # This version adds a "user" column.
        try:
            bot_db.execute(
                f"ALTER TABLE {PLUGIN_NAME}_messages ADD COLUMN user VARCHAR(20);"
            )
        except db.OperationalError:
            pass
        bot_db.set_plugin_value(PLUGIN_NAME, "db_version", 1)
        running_version = 1

    if running_version < 2:
        # This version removes the "user" column. This does NOT work on sqlite3.
        try:
            bot_db.execute(f"ALTER TABLE {PLUGIN_NAME}_messages DROP COLUMN user;")
        except db.OperationalError:
            pass
        bot_db.set_plugin_value(PLUGIN_NAME, "db_version", 2)
        running_version = 2


@commands("demo-db-set")
def demo_db_set(bot, trigger):
    """
    Sets the parameters in a DB table
    """
    my_message = Message()
    my_message.contents = trigger.group(2)

    bot_db = db.SopelDB(bot.config)
    db_session = bot_db.session()
    db_session.add(my_message)
    try:
        db_session.commit()
        bot.reply(f"stored '{trigger.group(2)}' in the DB")
    except db.SQLAlchemyError:
        bot.reply("Failed to store in the DB")
        db_session.rollback()
    db_session.close()


@commands("demo-db-get")
def demo_db_get(bot, trigger):  # pylint: disable=unused-argument
    """
    Sets the parameters in a DB table
    """
    bot_db = db.SopelDB(bot.config)
    db_session = bot_db.session()
    try:
        message = db_session.query(Message).order_by(Message.id.desc()).first()
        db_session.delete(message)
        db_session.commit()
        bot.reply(f"deleted '{message.contents}' from the DB")
    except db.SQLAlchemyError:
        bot.reply("Failed to read/delete from the DB")
        db_session.rollback()
    db_session.close()


# Timer
@commands("demo-test-timer")
def demo_start_timer(bot, trigger):
    """
    This demostrates how to start a timer, reset it (twice) and then let it trigger.
    """
    my_timer = demo_timer(bot, trigger.sender.lower())
    my_timer.start()
    bot.reply("The timer started!")
    sleep(2)
    my_timer.reset(start=True)
    bot.reply("The timer reset 1/2")
    sleep(2)
    my_timer.reset(start=True)
    bot.reply("The timer reset 2/2")


@timer.resetable_timer(3)
def demo_timer(bot, sender=None):
    """ Sends a message to the sender after 3 seconds """
    bot.say("Timer fired!", sender)
    my_timer = demo_timer(bot, sender)
    my_timer.start()


@commands("demo-test-timer-change")
def demo_start_timer_changeable(bot, trigger):
    """
    This demostrates how to start a timer, with a variable call back time.
    """
    my_timer = demo_timer(bot, trigger.sender.lower())

    if not trigger.group(2):
        my_timer.reset(start=True)
        return

    bot.say(f"The timer is set on {trigger.group(2)}")
    my_timer.reset(start=True, interval=int(trigger.group(2)))


# Class instance
@commands("demo-counter")
def demo_counter(bot, _):
    """
    This uses the Demo class instance that was initialized in the `setup`. It increases its counter
    and announces its value
    """
    demo_instance = bot.memory["demo"]
    demo_instance.counter += 1
    bot.reply(f"The counter is now {demo_instance.counter}")


@commands("demo-reset-counter")
def demo_reset_counter(bot, trigger):
    """
    This resets the Demo class instance counter
    """
    bot.memory["demo"].reset_counter(bot, trigger.sender)
