"""
Woggle - bot framework

  Copyright (C) 2024 Liam ScoutLink <liam1@scoutlink.net>
  Copyright (C) 2024 Mobius <18geeo@gmail.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from helpers import timer

class AutoModerate:
    
    """
    
    [automoderate]
    automoderate = True
    autointerval = 60
    hatmoderate = False
    exemptmoderatechannels = #chanops
    requiredprivilage = 1
    serviceshost = services.scoutlink.net
    
    [permissions]
    automoderate = thijseigenwijs
    """
    
    
    def __init__(self, bot):
        print("[AutoModerate] Initializing plugin")
        self.opschannel = bot.config.JotiGeneral.ops_channel
        self.automoderate_timer = automoderateTimer(bot)
        self.automoderate_timer.reset(True, int(bot.config.automoderate.autointerval))
    
    
    def check_automoderate(self, bot):
        for chan in bot.channels:
            self.check_automoderate_channel(bot, chan)
    
    def check_automoderate_channel(self, bot, chan):

        # if it is already +m no point checking
        if "m" in bot.channels[chan].modes:
            return
        
        # check if channel is excempt
        if chan in bot.config.automoderate.exemptmoderatechannels:
                return
        
        isOp = False
        
        if bot.config.automoderate.hatmoderate:
            # Check if there is a hatted user
            privileges = bot.channels[chan].privileges
            for user in privileges:
                if user in bot.config.automoderate.exemptusers or user == bot.nick:
                    continue
                if privileges[user] >= int(bot.config.automoderate.requiredprivilage):
                    # make sure ChanServ is not marked as an operator
                    if bot.channels[chan].users[user].host != bot.config.automoderate.serviceshost:
                        isOp = True
                        break
        else:
            # check if there is a shared user between here and the ops channel
            for user in bot.channels[self.opschannel].users:
                # skip if bot
                if user == bot.nick:
                    continue
                # skip if in exempt list:
                if user in bot.config.automoderate.exemptusers:
                    continue

                if user in bot.channels[chan].users:
                    # make sure ChanServ is not marked as an operator
                    if bot.channels[chan].users[user].host != bot.config.automoderate.serviceshost:
                        isOp = True
                        break

        if not isOp:
            bot.say("[AutoModerate] No op in " + chan + " setting +m", self.opschannel)
            bot.write(["MODE", chan, "+m"])

AUTOMODERATE = None

def setup(bot):
    global AUTOMODERATE
    AUTOMODERATE = AutoModerate(bot)
    
@timer.resetable_timer(60)
def automoderateTimer(bot):
    global AUTOMODERATE
    AUTOMODERATE.automoderate_timer.reset(True, None)
    AUTOMODERATE.check_automoderate(bot)
    