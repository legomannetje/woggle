"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from helpers import auth, sa
from sopel import plugin
import time

class RegisterChannels:
    """
    This is the class to register channels. This is one of the JotiGeneral
    plugins to only be used during joti.

    Config:

    [JotiGeneral]
    ops_channel = #beneluxops
    bot_channel = #dutch-bots
    coord = max
    cos = Thijseigenwijs, Vaandrig
    ops = Suikerklontje, zeilertje
    sups = None
    chanserv = chanserv
    operserv = operserv
    corights = "+votsrihqaRAF"
    oprights = "+varioe"
    suprights = "+Vvi"
    channels = #dutch01, #dutch02, #dutch03

    [RegisterChan]
    default_topic = "ScoutLink #dutch JOTI Channel (CHANNEL CLOSED)"
    chanmode = "+dsplitn 1 override"

    [permissions]
    registerchan = thijseigenwijs, Vaandrig

    """
    
    def __init__(self, bot):
        self.botchan = bot.config.JotiGeneral.bot_channel
        self.botchantt = bot.config.JotiGeneral.bot_channel
        self.chanserv = bot.config.JotiGeneral.chanserv
        self.channels = self.channels = bot.config.JotiGeneral.channels.translate(
            str.maketrans("", "", " \n\t\r")
        ).split(",")
        
        print(self.channels)
        print("[RegisterChannels] Setup complete")
        return
    
    
    def register_channel(self, bot, trigger, channel, forced=False):
        # Create the channel
        if not channel.startswith("#"):
            bot.notice("Please supply a channel, starting with the '#'")
            return
        
        bot.say(f"Joining {channel}", self.botchan)
        bot.join(channel)
        sa.sajoin(bot, trigger.nick, channel)
        bot.say(f"[CHANREG] {trigger.nick} is registering {channel}", self.botchantt)
        
        bot.write(['MODE', channel, '+o', bot.nick])
        bot.say("register {}".format(channel), self.chanserv)
        
        bot.write(["MODE", channel, bot.config.RegisterChan.chanmode])
        bot.write(["TOPIC", channel, bot.config.RegisterChan.default_topic])
        bot.say("set {} mlock off".format(channel), self.chanserv)
        
        bot.say(
            "flags {} {} {}".format(
                channel,
                bot.config.JotiGeneral.coord,
                bot.config.JotiGeneral.corights,
            ),
            self.chanserv,
        )
        
        return
    
    
    def deregister_channel(self, bot, trigger, channel):
                        
        bot.say("DEREGCHAN | Kicked all users from the channel", self.botchan)
        bot.say("/cs fdrop {}".format(channel), trigger.nick)
        
        users = bot.channels[channel].users
        priv_users = bot.channels[self.botchan].users
        
        for user in users:
            if(user not in priv_users):
                bot.write(['SAPART', user, channel, " :Closing the channel"])      
                
        bot.write(["MODE", channel, bot.config.RegisterChan.chanmode])
        bot.part(channel)
        for user in priv_users:
            bot.write(['SAPART', user, channel, " :Closed the channel"])
            
        bot.say(f"DEREGCHAN | Dropped {channel}",self.botchan)
        bot.say(f"DEREGCHAN | Dropped {channel}",self.botchantt)
                
        return
    
    
    def cmd_single(self, bot, trigger, register=True, override=False):
        return
    
    
    def cmd_multi(self, bot, trigger, register=True, override=False):
        return
    
    def cmd_all(self, bot, trigger, register=True, override=False):
        for channel in self.channels:
            if(register):
                self.register_channel(bot, trigger, channel)
                time.sleep(60)
            else:
                self.deregister_channel(bot, trigger, channel)
            
        return
    
    def cmd_join_all(self, bot, trigger):
        for channel in self.channels:
            bot.join(channel)
        return
    
    
    
REGISTER = None

def setup(bot):
    global REGISTER
    REGISTER = RegisterChannels(bot)
    
@plugin.commands("register-all")
def register_all(bot, trigger):
    global REGISTER
    if(trigger.sender == REGISTER.botchan):
        REGISTER.cmd_all(bot, trigger, register=True)
    else:
        bot.say(f"[REGCHAN] {trigger.nick} tried to register all the channels, but isn't doing that in the right channel", REGISTER.botchantt)
        bot.say(f"[REGCHAN] {trigger.nick} tried to register all the channels, but isn't doing that in the right channel", REGISTER.botchan)
        
        
@plugin.commands("deregister-all")
def deregister_all(bot, trigger):
    global REGISTER
    if(trigger.sender == REGISTER.botchan):
        REGISTER.cmd_all(bot, trigger, register=False)
    else:
        bot.say(f"[REGCHAN] {trigger.nick} tried to deregister all the channels, but isn't doing that in the right channel", REGISTER.botchantt)
        bot.say(f"[REGCHAN] {trigger.nick} tried to deregister all the channels, but isn't doing that in the right channel", REGISTER.botchan)
        
        
@plugin.commands("join-all")
def join_all(bot, trigger):
    global REGISTER
    REGISTER.cmd_join_all(bot, trigger)