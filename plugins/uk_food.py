"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from helpers import auth
from sopel import plugin
import random

@plugin.commands("chocolate")
def chocolate(bot, trigger):
    bot.action(
        "gives {} a big bar of Caramel Seasalt Tony Chocolonely!".format(getName(trigger))
    )


@plugin.commands("pie")
def pie(bot, trigger):
    slagroom = random.choice([" with a plume of wipped cream", "", ""])
    bot.action(
        "gives {} a big piece of applepie!".format(getName(trigger), slagroom)
    )


@plugin.commands("cookie")
def cookie(bot, trigger):
    cookie_type = random.choice(["Chocolate Chip ", "Oreo ", "Glace ", "", "Hello Kitty ","Biscuit"])
    size = random.choice(["small", "normal", "big", "gigantic"])
    flavor = random.choice(["tasty", "", "amazing","horrible"])
    method = random.choice(["bakes for", "gives to", "buys for"])
    bot.action(
        "{} {} een {} {} {}koek!".format(
            method, getName(trigger), flavor, size, cookie_type
        )
    )


@plugin.commands("horse")
def horse(bot, trigger):
    bot.action(
        "starts a fire to grill the horse on for {}".format(getName(trigger))
    )

@plugin.commands("crisps")
def crisps(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a bag of crisps".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a bag of crisps".format(trigger.nick))


@plugin.commands("peanuts")
def peanuts(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a huge bucket of peanuts".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("throws {} a small container filled with packing peanuts".format(trigger.nick))


@plugin.commands("jerky")
def jerky(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a big bag of spicy jerky".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("gives {} a small bag of dried hippo meat".format(trigger.nick))


@plugin.commands("meatballsub")
def meatballsub(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a tasty meatball sub!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} a tasty saucedripping meatball sub!".format(trigger.nick)
        )


@plugin.commands("tortillacrisps")
def tortillacrisps(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a big bucket filled with salty tortillacrisps".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} a big bucket filled with tortillacrisps...".format(
                trigger.nick
            )
        )


@plugin.commands("mars")
def mars(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a Mars bar".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a Mars bar".format(trigger.nick))


@plugin.commands("twix")
def twix(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action("gives {} from {} a Twix".format(trigger.group(2), trigger.nick))
    else:  # Give to yourself
        bot.action("gives {} a Twix".format(trigger.nick))


@plugin.commands("chocolateorange")
def chocolateorange(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a piece of their chocolate orange".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("slams a chocolateorange on the table for {} ".format(trigger.nick))


@plugin.commands("tosti", "tostie")
def tosti(bot, trigger):

    if int(random.random() * 100) == 50:
        bot.action(
            "sorry {}, there is a horse on the fire so you have to wait for a bit!".format(
                trigger.nick
            )
        )
        return

    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a grilled cheese... ".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {}a nice hot grilled cheese... Dont forget the !ketchup! ".format(
                trigger.nick
            )
        )


@plugin.commands("cheese")
def cheese(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a big pile of Gouda Cheese. ".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} a castle made out of English Cheddar.".format(
                trigger.nick
            )
        )


@plugin.commands("salsadip", "salsa")
def salsa(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a big bucket of salsadip".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("gives {} a small half eaten container of cheap salsa".format(trigger.nick))


@plugin.commands("sausage")
def sausage(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a few slices of a big smoked sausage... Amazing with Mustard!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} a few slices of a dried tasty sausage... Dont eat this with mustard!".format(
                trigger.nick
            )
        )



@plugin.commands("mixednuts")
def mixednuts(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "puts for {} a big pile of mixed nuts on the bar in courtesy of {} ... And dont sort!!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "puts for {} a small pile of mixed nuts on the bar... Dont sort!".format(
                trigger.nick
            )
        )


@plugin.commands("mayonaise", "mayo")
def mayonaise(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} a small dot of mayonaise for {}... ".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} a big pile of mayonaise... !".format(
                trigger.nick
            )
        )


@plugin.commands("curry")
def curry(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} some curry meant for {}... with real pieces of Marie Curie afcourse!!!".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} a big bowl of curry...".format(
                trigger.nick
            )
        )


@plugin.commands("ketchup")
def ketchup(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} some ketchup to share with {}....".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "gives {} some ketchup which they themselves can eat all...".format(
                trigger.nick
            )
        )


@plugin.commands("sugar")
def sugar(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a small bag of powdered sugar...".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("gives {} a Eiffeltower made from sugarcubes...".format(trigger.nick))


@plugin.commands("pizza")
def pizza(bot, trigger):
    pizzarange = [
        "pizza hawaii",
        "pizza salami",
        "pizza quattro formage",
        "pizza margaritta",
        "pizza shoarma",
        "pizza fungi",
    ]
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} a pizza to share with {} a {}".format(
                trigger.group(2), trigger.nick, random.choice(pizzarange)
            )
        )
    else:  # Give to yourself
        bot.action("gives {} a {}".format(trigger.nick, random.choice(pizzarange)))


@plugin.commands("chips")
def chips(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a steaming pile of chips".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a small pile of chips".format(trigger.nick))




"""
   Fris Bar
"""


@plugin.commands("water")
def water(bot, trigger):
    bot.action("gives {} a big glass of icecold water... OH THIS IS FROZEN!!".format(getName(trigger)))


@plugin.commands("chocolatemilk", "chocomel")
def chocolatemilk(bot, trigger):
    tempature = random.choice(
        ["warm", "hot", "warm", "warm", "hot", "warm", "icecold"]
    )
    cream = random.choice(
        [" with a pile of wipped cream", " with small Legoman formed marshmallows", "", "", " with a straw", ""]
    )
    bot.action(
        "gives {} a {} chocolatemilk{}!".format(getName(trigger), tempature, cream)
    )


@plugin.commands("ijsthee", "icetea", "icethee")
def ijsthee(bot, trigger):
    bot.action("gives {} a nice glass of super hot disgusting IceTea".format(trigger.nick))


@plugin.commands("Coffee")
def Coffee(bot, trigger):
    temperature = random.choice(["hot", "warm"])
    coffee = random.choice(
        ["Cappuccino", "Espresso", "Coffee Wrong", "Coffee", "Senseo","Douwe Egberts"]
    )
    chance = random.randint(0, 25)
    if chance == 0:
        bot.action(" Error 418: I'm a teapot!")
    else:
        bot.action("gives {} a {} {}".format(getName(trigger), temperature, coffee))


@plugin.commands("tea")
def tea(bot, trigger):
    temperature = random.choice(["warm", "hot","perfect"])
    size = random.choice(["big", "normal", "small"])
    tea = random.choice(
        [
            "chai tea",
            "english breakfast",
            "Earl Grey",
            "sleepytime",
            "green thee",
            "yerba mate",
            "rooibos tea",
            "bubble tea",
            "thai tea",
        ]
    )

    bot.action("gives {} a {} {} {}".format(getName(trigger), size, temperature, tea))


@plugin.commands("coughsyrup")
def coughsyrup(bot, trigger):
    bot.action("gives {} a shot of strong coughsyrup".format(getName(trigger)))


@plugin.commands("Dettol")
def dettol(bot, trigger):
    bot.action(
        "pours a big cup of pure dettol and bleach in for {} . Would you like a slice of lemon with this?".format(
            getName(trigger)
        )
    )


@plugin.commands("coffeemilk")
def coffeemilk(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} to share with {} a cup of coffeemilk".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("gives {} a cup of oatmilk...".format(trigger.nick))

@plugin.commands("spoon")
def spoon(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} to share with {} a small teaspoon".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a huge spoon ".format(trigger.nick))


@plugin.commands("milk")
def milk(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} for {} a cup of cold milk...".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action("gives {} a cup of sour milk...".format(trigger.nick))


@plugin.commands("cola")
def cola(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} for {} a can of pepsi...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a cold glass bottle of coke...".format(trigger.nick))


@plugin.commands("fanta")
def fanta(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} for {} a glass of fanta...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a can of fanta...".format(trigger.nick))


@plugin.commands("7up", "sprite")
def sevenup(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} for {} a can of sprite...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a glass of sprite...".format(trigger.nick))

@plugin.commands("cassis")
def cassis(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a can of cassis...".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a glass of cassis...".format(trigger.nick))






@plugin.commands("applejuice")
def applejuice(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} a glass filled with applejuice".format(trigger.group(2), trigger.nick)
        )
    else:  # Give to yourself
        bot.action("gives {} a juicepouch filled with applejuice".format(trigger.nick))




@plugin.commands("orangejuice")
def orangejuice(bot, trigger):
    if trigger.group(2):  # Give cake to someone else
        bot.action(
            "gives {} from {} some orangejuice (this is a word for scrabble)".format(
                trigger.group(2), trigger.nick
            )
        )
    else:  # Give to yourself
        bot.action(
            "pours orangejuice all over {} )".format(
                trigger.nick
            )
        )


@plugin.commands("coconutwater")
def coconutwater(bot, trigger):
    bot.action("gives {} a weird look and starts looking for a coconut".format(getName(trigger)))


def getName(trigger):
    name = ""
    if trigger.group(2):  # Give cake to someone else
        name = trigger.group(2)
    else:  # Give to yourself
        name = trigger.nick

    return name
