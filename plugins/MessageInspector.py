"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin

class MessageInspector:
    """
    There is a single class that inspects ALL messages coming messages for various things
    Only the JID Detection is done in an other plugin
    
    
    [swearwords]
    enabled = True
    swearwords = 
    """
    
    
    
    def __init__(self, bot):
        self.swearEnabled = bot.config.swearwords.enabled
        if(self.swearEnabled)
            print("[Swear] Plugin initializing")
        