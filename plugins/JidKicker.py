"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin

"""
There is no config for this module. It just works. Also, no excemptions
"""

# Dictionary to track the first message status for users in each channel after joining
user_first_message = {}

@plugin.event('JOIN')
@plugin.rule('.*')
def track_user_join(bot, trigger):
    channel = trigger.sender
    user = trigger.nick

    # Initialize the user's first message status when they join a channel
    user_first_message[(channel, user)] = True

@plugin.rule('.*')
@plugin.require_chanmsg
def check_for_jid(bot, trigger):
    channel = trigger.sender
    user = trigger.nick

    # Only process the first message after joining
    if user_first_message.get((channel, user), False):
        # Mark that the user has now sent their first message
        user_first_message[(channel, user)] = False

        print(f"User {trigger.nick}'s first words are {trigger.args[1].lower()}")
        print('jid' in trigger.args[1].lower())

        # Check if "JID" is in the message
        if 'jid' in trigger.args[1].lower():
            bot.write(["KICK", trigger.sender, user, " :NOJID4U! ~The staff"])
            bot.say(f"{user} said a bad word", trigger.sender)

@plugin.event('PART')
@plugin.event('QUIT')
@plugin.rule('.*')
def clean_up_user(bot, trigger):
    """Remove user from tracking when they leave or quit."""
    user = trigger.nick
    for channel in list(user_first_message.keys()):
        if channel[1] == user:
            del user_first_message[channel]
