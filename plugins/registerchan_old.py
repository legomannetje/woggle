"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from helpers import auth, sa
from sopel import plugin
import time

class RegisterChannels:
    """
    This is the class to register channels. This is one of the JotiGeneral
    plugins to only be used during joti.

    Config:

    [JotiGeneral]
    ops_channel = #beneluxops
    bot_channel = #dutch-bots
    coord = max
    cos = Thijseigenwijs, Vaandrig
    ops = Suikerklontje, zeilertje
    sups = None
    chanserv = chanserv
    operserv = operserv
    corights = "+votsrihqaRAF"
    oprights = "+varioe"
    suprights = "+Vvi"
    channels = #dutch01, #dutch02, #dutch03

    [RegisterChan]
    default_topic = "ScoutLink #dutch JOTI Channel (CHANNEL CLOSED)"
    chanmode = "+dsplitn 1 override"

    [permissions]
    registerchan = Thijseigenwijs, Vaandrig

    """

    def deregister_channel(self, bot, trigger, override=False):
        """
        This is the function that will deregister the channel given as parameter
        """
        
        # Make the variables to store things we need to save
        channel_to_deregister = trigger.group(2)
        botchan = bot.config.JotiGeneral.bot_channel
        botchantt = bot.config.TTLogger.bot_channel
        chanserv = bot.config.JotiGeneral.chanserv
        
        if (botchan != trigger.sender) and override is False:
            # Give a notice to the user that they are not allowed to do this from there
            bot.notice(
                "DEREGCHAN | ERROR | Sorry, you are not allowed to do that here. \
Please use {} or contact TechTeam if you believe this is a mistake.".format(
                    botchan
                )
            )
            # Report it in the bot channel
            bot.say(
                "DEREGCHAN | Denied {} to deregister channel {}. Not in this channel.".format(
                    trigger.nick, channel_to_deregister
                ),
                botchan,
            )
            # Send a message to TT for debugging as well
            bot.say(
                "DEREGCHAN | Denied {} to deregister channel {}. Action done from {}".format(
                    trigger.nick, channel_to_deregister, trigger.sender
                ),
                botchantt,
            )
            return
        
        # Check if we supply a channel that starts with a #
        if not channel_to_deregister.startswith("#"):
            bot.notice("Please supply a channel, starting with the '#'")
            return

        if (
            channel_to_deregister not in bot.config.JotiGeneral.channels
            and override is False
        ):
            bot.notice(
                "DEREGCHAN | ERROR | {} is not in my control list. Please edit the configfile \
                if you want to deregister this channel".format(
                    channel_to_deregister
                )
            )
            return

        users = bot.channels[channel_to_deregister].users
        priv_users = bot.channels[botchan].users
        
        for user in users:
            if(user not in priv_users):
                bot.write(['SAPART', user, channel_to_deregister, " :Closing the channel"])
                
        bot.say("DEREGCHAN | Kicked all users from the channel",botchan)
        bot.say("fdrop {}".format(channel_to_deregister), chanserv)
        bot.part(channel_to_deregister)
        for user in priv_users:
            bot.write(['SAPART', user, channel_to_deregister, " :Closed the channel"])
            
        bot.say(f"DEREGCHAN | Dropped {channel_to_deregister}",botchan)
        
        
        
    def register_channel(self, bot, trigger, override=False):
        """
        This is the function that will register the channel given as parameter
        """
        
        # Make the variables to store things we need to save
        channel_to_register = trigger.group(2)
        botchan = bot.config.JotiGeneral.bot_channel
        botchantt = bot.config.TTLogger.bot_channel
        chanserv = bot.config.JotiGeneral.chanserv

        # First check if the message came from the correct channel (Or override is activated)      
        if (botchan != trigger.sender) and override is False:
            # Give a notice to the user that they are not allowed to do this from there
            bot.notice(
                "REGCHAN | ERROR | Sorry, you are not allowed to do that here. \
Please use {} or contact TechTeam if you believe this is a mistake.".format(
                    botchan
                )
            )
            # Report it in the bot channel
            bot.say(
                "REGCHAN | Denied {} to register channel {}. Not in this channel.".format(
                    trigger.nick, channel_to_register
                ),
                botchan,
            )
            # Send a message to TT for debugging as well
            bot.say(
                "REGCHAN | Denied {} to register channel {}. Action done from {}".format(
                    trigger.nick, channel_to_register, trigger.sender
                ),
                botchantt,
            )
            return
        
        # Check if we supply a channel that starts with a #
        if not channel_to_register.startswith("#"):
            bot.notice("Please supply a channel, starting with the '#'")
            return

        if (
            channel_to_register not in bot.config.JotiGeneral.channels
            and override is False
        ):
            bot.notice(
                "REGCHAN | ERROR | {} is not in my control list. Please edit the configfile \
                if you want to register this channel".format(
                    channel_to_register
                )
            )
            return


        # Logging everything we do
        bot.say(
            "REGCHAN | {} is registering {}".format(trigger.nick, channel_to_register),
            botchan,
        )
        
        bot.say(
            "REGCHAN | {} is registering {}".format(trigger.nick, channel_to_register),
            botchantt,
        )

        # Create the channel
        bot.join(channel_to_register)

        # Sajoin the user into the channel and give a little info
        sa.sajoin(bot, trigger.nick, channel_to_register)
        bot.say(
            "REGCHAN | Welcome to {}, {}".format(channel_to_register, trigger.nick),
            channel_to_register,
        )
        bot.say("REGCHAN | Doing my stuff now...", channel_to_register)

        # Logging
        bot.say(
            "REGCHAN | Starting registration sequence on {}".format(
                channel_to_register
            ),
            botchan,
        )
        
        bot.say(
            "REGCHAN | Starting registration sequence on {}".format(
                channel_to_register
            ),
            botchantt,
        )

        # Starting the registration sequence
        bot.write(['MODE', channel_to_register, '+o', bot.nick])
        bot.say("register {}".format(channel_to_register), chanserv)
        bot.write(["MODE", channel_to_register, bot.config.RegisterChan.chanmode])
        bot.write(["TOPIC", channel_to_register, bot.config.RegisterChan.default_topic])
        bot.say("set {} mlock off".format(channel_to_register), chanserv)

        # Registering the coord as founder
        bot.say(
            "REGCHAN | Setting {} as founder to {}....".format(
                bot.config.JotiGeneral.coord, channel_to_register
            ),
            botchan,
        )
        bot.say(
            "flags {} {} {}".format(
                channel_to_register,
                bot.config.JotiGeneral.coord,
                bot.config.JotiGeneral.corights,
            ),
            chanserv,
        )

        # I'm done for now
        bot.say(
            "REGCHAN | Registration on {} is finished {}".format(
                channel_to_register, trigger.nick
            ),
            botchan,
        )
        bot.say(
            "REGCHAN | Registration on {} is finished {}".format(
                channel_to_register, trigger.nick
            ),
            botchantt,
        )
        

        # sa.sapart(bot, trigger.nick, channel_to_register, "Done registering")
 
REGISTER = None

def setup(bot):
    global REGISTER
    REGISTER = RegisterChannels()
    
    
@plugin.commands("regchan")
@auth.require_permission("registerchan")
def register(bot, trigger):
    global REGISTER
    REGISTER.register_channel(bot, trigger, override=False)
    
    
@plugin.commands("deregchan")
@auth.require_permission("registerchan")
def deregister(bot, trigger):
    global REGISTER
    REGISTER.deregister_channel(bot, trigger, override=False)