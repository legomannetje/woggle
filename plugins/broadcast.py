"""
Woggle - bot framework

  Copyright (C) 2024 Thijs Tops <git@thijstops.com>

This file is part of Woggle. Woggle is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, version 3.

Woggle is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sopel import plugin
from helpers import auth


class Broadcast:
    
    """
    
    [broadcast]
    prefix = None
    sayUser = False
    
    [permissions]
    broadcast = thijseigenwijs
    """
    
    
    def __init__(self, bot):
        print("[Broadcast] Initializing plugin")
        self.botchan = botchan = bot.config.JotiGeneral.bot_channel
    
    
    def send_broadcast(self, bot, trigger):
        # Check if there is a message to broadcast
        if not trigger.group(2):
            bot.notice("[BROADCAST] You do need to provide a message for me to broadcast")
            return 0
        
        # We have a message, logging it
        bot.say(f"[BROADCAST] {trigger.nick} has send the following broadcast \"{trigger.group(2)}\"")

        prefix = bot.config.broadcast.prefix
        mentionUser = bot.config.broadcast.mentionuser

        # Send it in all the channels we are in
        for chan in bot.channels:
            if(prefix is None):
                bot.say(trigger.group(2), chan)
            else:
                if mentionUser:
                    bot.say(prefix + " by " + trigger.nick + " : " + trigger.group(2), chan)
                else:
                    bot.say(prefix + " " + trigger.group(2), chan)
        # Send that we are done
        bot.notice("[BROADCAST] Done!", trigger.nick)
        return 0


BROADCAST = None

def setup(bot):
    global BROADCAST
    BROADCAST = Broadcast(bot)

@plugin.commands("broadcast")
@plugin.priority("low")
@plugin.example(".broadcast <announcement>")
@auth.require_permission("broadcast")
def broadcast(bot, trigger):
    """Broadcast a message to all the channels the bot is in."""
    global BROADCAST
    BROADCAST.send_broadcast(bot, trigger)
    